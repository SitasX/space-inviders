require 'shield'

function PlayerShield(x, y, radius)
	local shield = Shield(x, y, radius)
	shield.fixture:setUserData('player_shield')

	return shield
end