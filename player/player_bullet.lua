require 'bullet'

function PlayerBullet(x, y, angle)
	local bullet = Bullet(x, y, angle)
	bullet:addCollideType('npc_ship')
	bullet:addCollideType('garbage')

	return bullet
end