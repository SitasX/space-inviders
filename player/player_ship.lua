require "ship"
require "player/player_bullet"
require "player/player_shield"

function PlayerShip(x, y)
	local ship = Ship(x, y)

	-- Переменные для звука щита
	ship.sound_timer = 0
	ship.sound_state = ""
	ship.sound_play = sounds.shield_repeat:isPlaying()

	-- Скорость уменьшения энергии щита
	ship.deltaEnergy = 6

	-- Изображение прицела
	ship.cs = images.cross

	ship.type = 'player_ship'
	ship.fixture:setUserData('player_ship')

	function ship:setShieldEnabled(value)
		if value then
			if self.energy == 0 then return end
			if not self.shield then
				-- Воспроизведение звука щита
				ship.sound_state = "in"

				-- Создание щита вокру игрока
				ship.shield = PlayerShield(self.body:getX(), self.body:getY(), 50)
			end
		else
			if self.shield then
				for i, object in ipairs (objects) do
					if ship.shield == object then
						-- Остановка звуки щита
						love.audio.stop(sounds.shield)
						ship.sound_timer = 0
						ship.sound_state = ""

						-- Удаление щита
						ship.shield.fixture:destroy()
						table.remove(objects, i)
						ship.shield = nil
					end
				end
			end
		end
	end

	function ship:update(deltaTime)
		if self.shield then
			if self.energy > 0 then
				self.energy = self.energy - self.deltaEnergy * deltaTime
			else
				self.energy = 0
				self:setShieldEnabled(false)
			end
		end
		-- Звук щита
		if ship.sound_state == "in" then
			if ship.sound_timer == 0 then
				love.audio.play(sounds.shield)
			end
			ship.sound_timer = ship.sound_timer + deltaTime
			if ship.sound_timer > 28.4 then --поменять
				ship.sound_state = "rep"
				ship.sound_timer = 0
				love.audio.stop(sounds.shield)
			end
		elseif ship.sound_state == "rep" then
			if not ship.sound_play then
				love.audio.rewind(sounds.shield_repeat)--вырезать 0.1 и повторять 
		   		love.audio.play(sounds.shield_repeat)--воспроизвести звук, который надо повторить
			end
		end

		-- Смещение турели относительно координат центра коробля
		self.turelX = self.body:getX() - 20 * math.cos(self.body:getAngle())
		self.turelY = self.body:getY() - 20 * math.sin(self.body:getAngle())

		-- Ожидание подключения джойстика
		if not self.joystick then self.joystick = love.joystick.getJoysticks()[1] end

		-- Выбор управления в зависимости от наличия джойстика
		if self.joystick then
			self:joystickControl(deltaTime)
		else
			self:keyboardControl(deltaTime)
		end

		-- Перемещение щита вместе с кораблем
		if ship.shield then self.shield:setPosition( self.body:getX(), self.body:getY()) end

		-- Задержка между выстрелами
		self.shootTimer = self.shootTimer + deltaTime

		-- Следование камеры за кораблем
		camera.x = self.body:getX() - love.window.getWidth() * 0.5
		camera.y = self.body:getY() - love.window.getHeight() * 0.5

		-- Столкновение камеры с границами фона
		local maxCameraX = worldWidth - love.window.getWidth()
		local maxCameraY = worldHeight - love.window.getHeight()
		
		if camera.x < 0 then camera.x = 0 end
		if camera.y < 0 then camera.y = 0 end
		if camera.x > maxCameraX then camera.x = maxCameraX end
		if camera.y > maxCameraY then camera.y = maxCameraY end

		-- Возвращение корабля при вылете за границы
		if self.body:getX() < 50 then self.body:applyForce(self.movementSpeed * 4, 0) end
		if self.body:getY() < 50 then self.body:applyForce(0, self.movementSpeed * 4) end
		if self.body:getX() > worldWidth - 50 then self.body:applyForce(-self.movementSpeed * 4, 0) end
		if self.body:getY() > worldHeight - 50 then self.body:applyForce(0, -self.movementSpeed * 4) end
	end

	--[[ УПРАВЛЕНИЕ КЛАВИАТУРОЙ И МЫШЬЮ ]]
	function ship:keyboardControl(deltaTime)
		-- Включение/выключение щита на ПКМ
		if love.mouse.isDown("r") then
			self:setShieldEnabled(true)
		else
			self:setShieldEnabled(false)
		end
		
		-- Движение вперед и назад
		if love.keyboard.isDown("w") then self:move(1) end
		if love.keyboard.isDown("s") then self:move(-1) end

		-- Разворот корабля
		if love.keyboard.isDown("d") then
			self.body:applyAngularImpulse(self.rotationSpeed * deltaTime)
		end
		if love.keyboard.isDown("a") then
			self.body:applyAngularImpulse(-self.rotationSpeed * deltaTime)
		end

		-- Поворот турели за мышью и стрельба
		local mX,mY = camera:mousePosition()
		self.turelAngle = math.atan2(mY - self.turelY, mX - self.turelX)

		-- Стрельба на ЛКМ
		if love.mouse.isDown("l") then
			love.audio.play(sounds.player_shoot)
			self:shoot(PlayerBullet(self.turelX, self.turelY, self.turelAngle))
		end
	end

	--[[ УПРАВЛЕНИЕ ДЖОЙСТИКОМ ]]
	function ship:joystickControl(deltaTime)
		if not self.joystick then return end

		buttonCount = self.joystick:getButtonCount()

		local accuracy = 0.3

		local x1 = self.joystick:getAxis(1)
		local y1 = self.joystick:getAxis(2)

		local x2 = self.joystick:getAxis(self.joystick:getAxisCount() - 1)
		local y2 = self.joystick:getAxis(self.joystick:getAxisCount())

		-- Поворот и следование игрока в направлении левого стика
		local left = math.abs(x1) + math.abs(y1)
		if left > accuracy then
			local joystickAngle = self:validateAngle(math.atan2(y1, x1))

			local delta = self:validateAngle(joystickAngle - self:validateAngle(self.body:getAngle()))
		   	delta = ((delta + math.pi) % (math.pi * 2)) - math.pi

		   	if delta < 0 then
		   		self.body:applyAngularImpulse(-self.rotationSpeed * deltaTime)
			elseif delta > 0 then
				self.body:applyAngularImpulse(self.rotationSpeed * deltaTime)
			end

			self:move(math.min(left, 1))
		end

		-- Поворот и стрельба из турели
		local right = math.abs(x2) + math.abs(y2)
		if right > accuracy then
			local joystickAngle = self:validateAngle(math.atan2(y2, x2))
			self.turelAngle = joystickAngle

			if right > 0.95 then
				love.audio.play(sounds.player_shoot)
				self:shoot(PlayerBullet(self.turelX, self.turelY, self.turelAngle))
			end
		end

		-- Включение/выключение щита
		if self.joystick:getAxisCount() > 4 then -- Костыль для работы джойстика
			if self.joystick:isDown(6) then
				self:setShieldEnabled(true)
			else
				self:setShieldEnabled(false)
			end
		else
			if self.joystick:isGamepadDown('rightshoulder') then
				self:setShieldEnabled(true)
			else
				self:setShieldEnabled(false)
			end
		end
	end


	function ship:draw()
		-- Корпус
		love.graphics.draw(
			self.pic,
			self.body:getX(), self.body:getY(),
			self.body:getAngle(),
			1, 1,
			self.pic:getWidth() * 0.5, self.pic:getHeight() * 0.5
		)
		
		local mX,mY = camera:mousePosition()

		-- Турель
		love.graphics.draw(
			self.turel,
			self.turelX, self.turelY,
			self.turelAngle,
			1, 1,
			self.turel:getWidth() * 0.33, self.turel:getHeight() * 0.5
		)

		if not self.joystick then -- Mouse
			-- Прицел
			love.graphics.draw(
				self.cs,
				mX, mY,
				0, 0.5, 0.5,
				self.cs:getWidth() * 0.25, self.cs:getHeight() * 0.25
			)
		else -- Joystick
			--[[local count = self.joystick:getButtonCount()
			for i = 1, count do
				love.graphics.print(i .. ": " .. tostring(self.joystick:isDown(i)), self.body:getX(), self.body:getY() + 25 * i)
			end]]
		end
	end

	return ship
end










