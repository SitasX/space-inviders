objects = {}

--[[ ЗАГОТОВКИ МУСОРА ]]
local garbageTemplates = {}

-- trash-1.png
garbageTemplates[1] = {
	shape = love.physics.newPolygonShape(5, 42, 51, 76, 86, 41, 71, 14, 47, 5),
	pic = images.trash1
}

-- trash-2.png
garbageTemplates[2] = {
	shape = love.physics.newPolygonShape(5, 20, 5, 43, 31, 57, 51, 47, 58, 22, 24, 3),
	pic = images.trash2
}

-- trash-3.png
garbageTemplates[3] = {
	shape = love.physics.newPolygonShape(3, 16, 3, 30, 17, 37, 38, 29, 45, 18, 21, 2),
	pic = images.trash3
}

--frag1.png
garbageTemplates[4] = {
	shape = love.physics.newPolygonShape(1, 11, 25, 70, 59, 54, 62, 43, 46, 10, 25, 8),
	pic = images.frag1
}

--frag2.png
garbageTemplates[5] = {
	shape = love.physics.newPolygonShape(2, 10, 5, 40, 17, 63, 35, 51, 39, 41, 35, 2, 18, 5, 8, 5),
	pic = images.frag2
}

-- frag3.png
garbageTemplates[6] = {
	shape = love.physics.newPolygonShape(8, 58, 15, 83, 37, 104, 117, 74, 119, 56, 92, 16, 65, 7),
	pic = images.frag3
}

--[[ ГЕНЕРАЦИЯ МУСОРА ]]
function Garbage(x, y, angle)
	local g = {}
	g.type = 'garbage'

	-- Прочность
	g.health = 30
	g.isDead = false

	-- Тип мусора
	local garbageType = math.random(1, table.getn(garbageTemplates))

	-- Изображение
	g.pic = garbageTemplates[garbageType].pic

	-- Физика
	g.body = love.physics.newBody(world, x or 0, y or 0, "dynamic")
	g.shape = garbageTemplates[garbageType].shape
	g.fixture = love.physics.newFixture(g.body, g.shape)
	g.fixture:setUserData('garbage')

	g.body:setLinearDamping(0.5)
	g.body:setAngularDamping(0.2)

	function g:destroy()
		for i, g in ipairs(objects) do
			if g == self then 
				table.remove(objects, i)
				self = nil
			end
		end
	end

	function g:hit(angle, value)
		local ik = value * 3
		local ix, iy = math.cos(angle) * ik, math.sin(angle) * ik
		self.body:applyLinearImpulse(ix, iy)

		-- Звук урона
		if sounds.garbage_damage:isPlaying() then
			love.audio.play(sounds.garbage_damage1)
		else
			love.audio.play(sounds.garbage_damage)
		end

		if not self.isDead then -- если еще не мертв
			-- Толкаем корабль
			self.health = self.health - value -- отнимаем от прочности value
			if (self.health <= 0) then -- если прочность <= 0
				self.isDead = true -- корабль "умирает"
				self:destroy()
			end
		end
	end

	function g:update()
		if self.body:getX() < -100 then self:destroy() return end
		if self.body:getY() < -100 then self:destroy() return end
		if self.body:getX() > worldWidth + 100 then self:destroy() return end
		if self.body:getY() > worldHeight + 100 then self:destroy() return end
	end

	function g:draw()
		love.graphics.draw(
			self.pic,
			self.body:getX(),
			self.body:getY(),
			self.body:getAngle()
		)
	end

	return g
end