function Bullet(x, y, angle)
	local bullet = {
		speed = 900,
		x = x or 0,
		y = y or 0,
		angle = angle or 0,
		collideTypes = {},
		power = 10 -- Наносимый урон
	}

	-- Изображения
	bullet.pic = images.bullet

	function bullet:addCollideType(type)
		table.insert(self.collideTypes, tostring(type))
	end

	function bullet:destroy()
		for i, bullet in ipairs(objects) do
			if bullet == self then
				table.remove(objects, i)
				self = nil
			end
		end
	end

	function bullet:update(dt)
		-- Движение пули в заданном углом 'angle' направлении
		self.x = self.x + (self.speed * dt) * math.cos(self.angle)
		self.y = self.y + (self.speed * dt) * math.sin(self.angle)

		-- Уничтожение при выходе заграницы фона
		if self.x < 0 then self = nil return end
		if self.x > worldWidth then self = nil return end		
		if self.y < 0 then self = nil return end
		if self.y > worldHeight then self = nil return end

		-- Столкновение с объектами
		for i, object in ipairs(objects) do
			-- Если тип корабля есть в списке типов для столкновения
			for j, type in ipairs(self.collideTypes) do
				if object.type == type then
					if object.fixture:testPoint(self.x + (self.speed * dt) * math.cos(self.angle), self.y + (self.speed * dt) * math.sin(self.angle)) then
						if object.hit then -- Если у объекта есть функция нанесения урона
							object:hit(self.angle, self.power) -- Наносим объекту урон
						end

						self:destroy()
						return
					end
				end
			end
		end
	end

	function bullet:draw()
		love.graphics.draw(
			self.pic,
			self.x, self.y,
			self.angle,
			1, 1,
			self.pic:getWidth() - 24, self.pic:getHeight() + 1)

		love.graphics.draw(
			self.pic,
			self.x, self.y,
			self.angle,
			1, 1,
			self.pic:getWidth() - 24, self.pic:getHeight() - 7)
	end

	return bullet
end