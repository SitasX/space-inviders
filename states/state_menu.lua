require 'states/state'
require 'button'

state_menu = State()
states:add("menu", state_menu)

function state_menu:load()
	love.mouse.setVisible(true)

	self.buttonStart = Button(love.graphics.getWidth() * 0.5, love.graphics.getHeight() * 0.5 - 32,  "Start")
	self.buttonExit = Button(love.graphics.getWidth() * 0.5, love.graphics.getHeight() * 0.5 + 16, "Exit")
	self.escapePressed = love.keyboard.isDown("escape")

	self.repeatBackX = math.floor(love.graphics.getWidth() / images.stars_small:getWidth() + 0.5)
	self.repeatBackY = math.floor(love.graphics.getHeight() / images.stars_small:getHeight() + 0.5)
	self.noiseX = math.random(0, 500)
	self.noiseY = math.random(0, 500)
	self.noiseDistance = 150
end

function state_menu:update(deltaTime)
	local mx, my = love.mouse.getPosition()

	self.buttonStart:update(deltaTime, mx, my)
	self.buttonStart:onPressed(function() states:switch("game") end)

	self.buttonExit:update(deltaTime, mx, my)
	self.buttonExit:onPressed(function() love.event.quit() end)

	self.noiseX = self.noiseX + 0.1 * deltaTime
	self.noiseY = self.noiseY + 0.1 * deltaTime
end

function state_menu:draw()
	-- Фон
	state_game:repeatImage(images.back1, self.repeatBackX, self.repeatBackY)

	-- Плавающие звезды
	local noiseX, noiseY = love.math.noise(self.noiseX), love.math.noise(self.noiseY)
	local dx, dy = self.noiseDistance * noiseX, self.noiseDistance * noiseY

	love.graphics.translate(-dx, -dy)
	state_game:repeatImage(images.stars_small, self.repeatBackX, self.repeatBackY)
	love.graphics.translate(dx, dy)

	-- Надпись "Space Invaders"
	love.graphics.printf("Space Invaders",
			love.graphics.getWidth() * 0.5,
			love.graphics.getHeight() * 0.5 - 100,
			300, "center", 0, 1, 1, 150) -- width, align, angle, scaleX, scaleY, originX

	-- Кнопки
	self.buttonStart:draw()
	self.buttonExit:draw()
end

function state_menu:quit()
end