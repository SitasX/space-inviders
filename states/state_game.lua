objects = {}
require 'states/state'
require 'states/state_game_dead'

require 'camera'
require 'player/player_ship'
require 'npc/npc_ship'
require 'garbage'

state_game = State()
states:add("game", state_game)

-- Отключение столкновения кораблей с их щитами
function preSolve(a, b, coll)
	if a:getUserData() == 'player_ship' and b:getUserData() == 'player_shield' then
		coll:setEnabled(false)
	end

	if a:getUserData() == 'npc_ship' and b:getUserData() == 'npc_shield' then
		coll:setEnabled(false)
	end
end

-- Нанесение урона игроку при столкновении с мусором
function postSolve(a, b, coll, normalimpulse1, tangentimpulse1, normalimpulse2, tangentimpulse2)
	if a:getUserData() == 'garbage' and b:getUserData() == 'player_ship' then
		local damage = math.abs(normalimpulse1 / 100)
		if damage > 0.6 then
			player:hit(0, damage)
		end
	end
end

function state_game:spawnEnemy()
	local dir = math.random(1, 4)
	local dx, dy = 0, 0
	local x, y = camera.x + love.graphics.getWidth() * 0.5, camera.y + love.graphics.getHeight() * 0.5

	if dir == 1 then
		dy = -love.graphics.getHeight()
	elseif dir == 2 then
		dx = love.graphics.getWidth()	
	elseif dir == 3 then
		dy = love.graphics.getHeight()
	elseif dir == 4 then
		dx = -love.graphics.getWidth()
	end 	

	table.insert(objects, NpcShip(x + dx, y + dy))
end

function state_game:load()
	love.mouse.setVisible(false)
	
	-- Фон
	worldWidth = 2560
	worldHeight = 1440
	self.repeatBackX = math.floor(worldWidth / images.back1:getWidth() + 0.5)
	self.repeatBackY = math.floor(worldHeight / images.back1:getHeight() + 0.5)
	self.repeatNebulaX = math.floor(worldWidth / images.back2:getWidth() + 0.5)
	self.repeatNebulaY = math.floor(worldHeight / images.back2:getHeight() + 0.5)

	self.score = 0
	self.npcCount = 3
	self.minGarbageCount = 3
	self.maxGarbageCount = 6
	self.maxGarbageSize = 15

	-- Экран смерти
	self.state_game_dead = StateGameDead()
	self.state_game_dead:load()

	-- Физический мир
	world:setCallbacks(nil, nil, preSolve, postSolve)

	-- Игрок
	player = PlayerShip(worldWidth * 0.5, 400)	

	-- Генерация 3 NPC в случайных точках мира
	for i = 1, self.npcCount do
		self:spawnEnemy()
	end

	-- Добавление мусора
	for i = self.minGarbageCount, self.maxGarbageCount do
		local gx, gy = math.random(0, worldWidth), math.random(0, worldHeight)
		for j = 1, math.random(0, self.maxGarbageSize) do
			table.insert(objects, Garbage(gx + math.random(-5, 5), gy + math.random(-5, 5)))
		end
	end

	table.insert(objects, player)

	world:update(0.0166)
	sounds.music1:setVolume(0.5)
	love.audio.play(sounds.music1)
end

function state_game:update(deltaTime)
	-- Обновление физики
	world:update(math.min(deltaTime, 0.0166))
	
	-- Обновление кораблей
	for i, ship in ipairs(objects) do ship:update(deltaTime) end

	-- Обновление экрана смерти
	if player.isDead then
		love.mouse.setVisible(true)
		self.state_game_dead:update(deltaTime)
	end

	-- Выход в меню на 'escape'
	if love.keyboard.isDown("escape") then states:switch("menu") end
end

function state_game:repeatImage(image, repeatX, repeatY)
	for i = 0, repeatX do
		for j = 0, repeatY do
			local x, y = i * image:getWidth(), j * image:getHeight()
			love.graphics.draw(image, x, y)
		end
	end
end

function state_game:draw()
	camera:set()
	
	-- Рисование фона

	self:repeatImage(images.back1, self.repeatBackX, self.repeatBackY)
	self:repeatImage(images.back2, self.repeatNebulaX, self.repeatNebulaY)
	self:repeatImage(images.back3, self.repeatBackX, self.repeatBackY)
	self:repeatImage(images.back4, self.repeatBackX, self.repeatBackY)
	-- Рисование объектов
	for i, object in ipairs(objects) do object:draw() end

	-- Рисование экрана смерти
	if player.isDead then self.state_game_dead:draw() end

	camera:unset()

	local r,g,b,a = love.graphics.getColor()
	local guiX, guiY = 10, 10

	-- Полоса жизней игрока
	love.graphics.setColor(110, 110, 110)
	love.graphics.rectangle("fill", guiX + 24, guiY + 10, 130, 20)

	love.graphics.setColor(254, 25, 25)
	love.graphics.rectangle(
		"fill",
		guiX + 24, guiY + 10,
		1.30*player.health, 20)

	-- Полоса  энергии
	love.graphics.setColor(110, 110, 110)
	love.graphics.rectangle("fill", guiX + 24, guiY + 38, 130, 20)
	
	love.graphics.setColor(55, 44, 189)
	love.graphics.rectangle(
		"fill",
		guiX + 24, guiY + 38,
		1.30 * player.energy, 20)
	love.graphics.setColor(r,g,b,a)

	-- Гуи полос жизни и энергии щита
	love.graphics.draw(images.gui, guiX, guiY)

	love.graphics.printf("Score: " .. tostring(self.score), love.graphics.getWidth() - 32, 32, 200, "right", 0, 1, 1, 200)
end

function state_game:quit()
	if player.isDead then self.state_game_dead:quit() end

	for i in pairs(objects) do
		if objects[i].fixture then
			objects[i].fixture:destroy()
		end
		objects[i] = nil
	end
	love.audio.stop()
end