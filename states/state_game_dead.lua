require 'states/state'
require 'button'

function StateGameDead()
	local state_game_dead = State()

	function state_game_dead:load()
		self.buttonRestart = Button(
			love.graphics.getWidth() * 0.5,
			love.graphics.getHeight() * 0.5 - 32, "Restart")

		self.buttonExit = Button(
			love.graphics.getWidth() * 0.5,
			love.graphics.getHeight() * 0.5 + 16, "Menu")
	end

	function state_game_dead:update(deltaTime)
		-- Обновление кнопки
		local mx, my = love.mouse.getPosition()
		self.buttonExit:update(deltaTime, mx, my)
		self.buttonExit:onPressed(function() states:switch("menu") end)

		self.buttonRestart:update(deltaTime, mx, my)
		self.buttonRestart:onPressed(function() states:switch("game") end)
	end

	function state_game_dead:draw()
		camera:unset()

		-- Полупрозрачный фон
		local r,g,b,a = love.graphics.getColor()
		love.graphics.setColor(0, 0, 0, 150)
		love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
		love.graphics.setColor(r,g,b,a)

		-- Надпсь
		love.graphics.printf("Dead",
			love.graphics.getWidth() * 0.5,
			love.graphics.getHeight() * 0.5 - 100,
			100, "center", 0, 1, 1, 50) -- width, align, angle, scaleX, scaleY, originX

		-- Кнопочка
		self.buttonExit:draw()
		self.buttonRestart:draw()
		
		camera:set()
	end

	function state_game_dead:quit()
	end

	return state_game_dead
end