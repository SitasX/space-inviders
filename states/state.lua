states = { current = nil }

-- States constructor
function State()
	local state = { type = 'State' }
	function state:load() end
	function state:update(deltaTime) end
	function state:draw() end
	function state:quit() end
	function state:keyreleased(key) end
	return state
end

-- Add a state to manager by name
function states:add(name, state)
	if ('State' == state.type) then
		self[tostring(name)] = state
	else
		error("Trying to add not a 'State' object!")
	end
end

-- Switch to state by name
function states:switch(stateName)
	if (self[stateName]) then
		if (self.current) then self.current:quit() end
		self.current = self[stateName]
		self.current:load()
	end
end

-- Updates current state
function states:update(deltaTime)
	if (self.current) then
		self.current:update(deltaTime)
	end
end

-- Draws current state
function states:draw()
	if (self.current) then
		self.current:draw()
	end
end