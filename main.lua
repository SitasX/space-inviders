require 'resources'
require 'states/state_game'
require 'states/state_menu'


function love.load()
	-- Настройка окна
	local width, height = love.window.getDesktopDimensions(0)
	love.window.setMode(width, height, { fullscreen = true })
	

	math.randomseed(os.time())
	world = love.physics.newWorld(0, 0)

	states:switch("menu")
end

function love.update(deltaTime)
	states:update(deltaTime)
end

function love.draw()
	love.graphics.setFont(defaultFont)
	states:draw()
end