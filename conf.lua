function love.conf(t)
	t.title = ("Space Invaders")
	t.window.icon = "assets/pictures/cs.png"
	
	--t.window.fullscreen = true
	t.modules.window = true
	
	t.window.vsync = true
	t.window.fsaa = 0

	t.window.fullscreentype = "normal"
	t.window.borderless = false
	t.window.resizable = false
	t.window.minwidth = 1
	t.window.minheight = 1
	t.console = false 
	

end