function Shield(x, y, radius)
	local shield = {}
	shield.type = 'shield'

	shield.body = love.physics.newBody(world, x, y, "kinematic")
	shield.shape = love.physics.newCircleShape(radius)
	shield.fixture = love.physics.newFixture(shield.body, shield.shape)
	shield.fixture:setUserData('shield')

	function shield:setPosition(x, y)
		self.body:setPosition(x, y)
	end

	function shield:update(deltaTime)
	end

	function shield:draw()
		love.graphics.circle("line", self.body:getX(), self.body:getY(), self.shape:getRadius())
	end

	table.insert(objects, shield)
	return shield
end