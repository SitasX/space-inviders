sounds = {}
images = {}

defaultFont = love.graphics.newFont("assets/font.ttf", 30)

sounds.player_shoot = love.audio.newSource("assets/sounds/shoot_good.wav", 'static')
sounds.enemy_shoot = love.audio.newSource("assets/sounds/shoot_evil.wav", 'static')
sounds.music1 = love.audio.newSource("assets/sounds/music1.mp3", 'static')
sounds.shield = love.audio.newSource("assets/sounds/shield.wav", 'static')
sounds.shield_repeat = love.audio.newSource("assets/sounds/shield_repeat.wav", 'static')
sounds.ship_damage = love.audio.newSource("assets/sounds/ship_damage.wav", 'static')
sounds.ship_damage1 = love.audio.newSource("assets/sounds/ship_damage.wav", 'static')
sounds.garbage_damage = love.audio.newSource("assets/sounds/ship_damage.wav", 'static')
sounds.garbage_damage1 = love.audio.newSource("assets/sounds/ship_damage.wav", 'static')

images.ship = love.graphics.newImage("assets/pictures/ship-good.png")
images.ship_evil = love.graphics.newImage("assets/pictures/ship-evil.png")
images.turel = love.graphics.newImage("assets/pictures/turel-good.png")
images.turel_evil = love.graphics.newImage("assets/pictures/turel-evil.png")
images.cross = love.graphics.newImage("assets/pictures/cs.png")
images.bullet = love.graphics.newImage("assets/pictures/bullet.png")
images.frag1 = love.graphics.newImage("assets/pictures/frag1.png")
images.frag2 = love.graphics.newImage("assets/pictures/frag2.png")
images.frag3 = love.graphics.newImage("assets/pictures/frag3.png")
images.trash1 = love.graphics.newImage("assets/pictures/trash-1.png")
images.trash2 = love.graphics.newImage("assets/pictures/trash-2.png")
images.trash3 = love.graphics.newImage("assets/pictures/trash-3.png")
images.back1 = love.graphics.newImage("assets/pictures/background/background.png")
images.back2 = love.graphics.newImage("assets/pictures/background/nebula-1.png")
images.back3 = love.graphics.newImage("assets/pictures/background/nebula-2.png")
images.back4 = love.graphics.newImage("assets/pictures/background/stars-1.png")
images.stars_small = love.graphics.newImage("assets/pictures/background/stars-small.png")
images.gui = love.graphics.newImage("assets/pictures/gui.png")
images.button = love.graphics.newImage("assets/pictures/button_green.png")
images.buttonHover = love.graphics.newImage("assets/pictures/button_green_hover.png")
