function Button(x, y, text)
	local button = {
		image = images.button,
		imageHover = images.buttonHover,

		x = x or 0,
		y = y or 0,
		text = text or "Sample Text",

		hovered = false,
		mouseDown = love.mouse.isDown('l'),
	}

	function button:onPressed(func)
		if self.hovered and love.mouse.isDown ("l") and not self.mouseDown then
			func()
		end
		self.mouseDown = love.mouse.isDown('l')
	end

	function button:update(deltaTime, mx, my)
		mx = mx + self.image:getWidth() * 0.5
		my = my + self.image:getHeight() * 0.5
		if mx > self.x and my > self.y and mx < self.x + self.image:getWidth() and my < self.y + self.image:getHeight() then
			self.hovered = true
		else
			self.hovered = false
		end
	end
	
	function button:draw()
		if not self.hovered then
			love.graphics.draw(self.image,
				self.x, self.y,
				0,
				1, 1,
				self.image:getWidth() * 0.5, self.image:getHeight() * 0.5)
		else
			love.graphics.draw(self.imageHover,
				self.x, self.y,
				0,
				1, 1,
				self.imageHover:getWidth() * 0.5, self.imageHover:getHeight() * 0.5)
		end

		love.graphics.printf(self.text,
			self.x, self.y,
			self.image:getWidth(), "center",
			0, 1, 1,
			self.image:getWidth() * 0.5, self.image:getHeight() * 0.5)
	end
	
	return button
end