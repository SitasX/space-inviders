require "ship"
require 'npc/npc_bullet'

function NpcShip(x, y)
	local ship = Ship(x, y)
	
	-- Картинки врага
	ship.pic = images.ship_evil
	ship.turel = images.turel_evil

	ship.type = 'npc_ship'
	ship.state = 'ожидание'
	ship.fixture:setUserData('npc_ship')

	function ship:destroy()
		for i, object in ipairs(objects) do
			if object == self then
				table.remove(objects, i)
				self = nil
				state_game.score = state_game.score + 10
				state_game:spawnEnemy()
			end
		end
	end

	function ship:update(deltaTime)
		-- Возвращение корабля при вылете за границы
		if self.body:getX() < 50 then self.body:applyForce(self.movementSpeed * 4, 0) end
		if self.body:getY() < 50 then self.body:applyForce(0, self.movementSpeed * 4) end
		if self.body:getX() > worldWidth - 50 then self.body:applyForce(-self.movementSpeed * 4, 0) end
		if self.body:getY() > worldHeight - 50 then self.body:applyForce(0, -self.movementSpeed * 4) end

		-- Поиск корабля игрока и получение его координат
		local playerX, playerY = -1, -1

		for i, ship in ipairs(objects) do
			if (ship.type == 'player_ship') then
				playerX = ship.body:getX()
				playerY = ship.body:getY()
			end
		end

		local distanceToPlayer = math.sqrt((self.body:getX() - playerX)^2 + (self.body:getY() - playerY)^2)

		-- Смещение турели
		self.turelX = self.body:getX() - 20 * math.cos(self.body:getAngle())
		self.turelY = self.body:getY() - 20 * math.sin(self.body:getAngle())

		-- Поворот турели за игроком
		self.turelAngle = self:validateAngle(math.atan2(playerY - self.body:getY(), playerX - self.body:getX()))

		-- Смена состояния в зависимости от условий
		if distanceToPlayer <= 200 then
			-- Отталкивание происходит вместе со стрельбой
			self:move(-1)
		elseif distanceToPlayer <= 250 then
			self.state = 'атака'
		elseif distanceToPlayer > 250 then
			self.state = 'преследование'
		end


		-- Поведение в зависимости от текущего состояния
		if self.state == 'преследование' then
			if playerX ~= -1 then

				local delta = self.turelAngle - self:validateAngle(self.body:getAngle())
	    	   	delta = ((delta + math.pi) % (math.pi * 2)) - math.pi

	    	   	if delta < 0 then
	    	   		self.body:applyAngularImpulse(- self.rotationSpeed * deltaTime)
	    		elseif delta > 0 then
	    			self.body:applyAngularImpulse(self.rotationSpeed * deltaTime)
	    		end

	    		self:move(1)
	    	end
		
		elseif self.state == 'атака' then
			love.audio.play(sounds.enemy_shoot)
			self:shoot(NpcBullet(self.turelX, self.turelY, self.turelAngle)) --стрельба в игрока
		end

		self.shootTimer = self.shootTimer + deltaTime
	end

	function ship:draw()
		-- Корпус
		love.graphics.draw(
			self.pic,
			self.body:getX(), self.body:getY(),
			self.body:getAngle(),
			1, 1,
			self.pic:getWidth() * 0.5, self.pic:getHeight() * 0.5
		)

		-- Турель
		love.graphics.draw(
			self.turel,
			self.turelX, self.turelY,
			self.turelAngle,
			1, 1,
			self.turel:getWidth() * 0.33, self.turel:getHeight() * 0.5
		)

		-- Полоса жизней
		local r,g,b,a = love.graphics.getColor()

		-- Фон
		love.graphics.setColor(110, 110, 110)
		love.graphics.rectangle("fill",
			self.body:getX() - 30, -- x
			self.body:getY() + 50, -- y
			60, 5) -- w, h

		-- Полоса
		love.graphics.setColor(39, 214, 44)
		love.graphics.rectangle("fill",
			self.body:getX() - 30, -- x
			self.body:getY() + 50, -- y
			(self.health * 0.01) * 60, 5) -- w, h
		love.graphics.setColor(r,g,b,a)
	end

	return ship
end