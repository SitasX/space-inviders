require 'bullet'

function NpcBullet(x, y, angle)
	local bullet = Bullet(x, y, angle)
	bullet:addCollideType('player_ship')
	bullet:addCollideType('garbage')
	bullet:addCollideType('shield')
	bullet.power = 2

	return bullet
end