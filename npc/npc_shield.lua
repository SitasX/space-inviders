require 'shield'

function NpcShield(x, y, radius)
	local shield = Shield(x, y, radius)
	shield.fixture:setUserData('npc_shield')

	return shield
end