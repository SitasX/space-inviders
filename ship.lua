function Ship(x, y)
	local ship = {}

	-- Изображения корпуса и турели (по умолчанию такие же как у игрока)
	ship.pic = images.ship
	ship.turel = images.turel

	-- Скорости
	ship.movementSpeed = 1600
	ship.rotationSpeed = 7000

	-- Физика
	ship.body = love.physics.newBody(world, x or 0, y or 0, "dynamic")
	ship.shape = love.physics.newCircleShape(ship.pic:getWidth() * 0.4)
	ship.fixture = love.physics.newFixture(ship.body, ship.shape)
	ship.body:setLinearDamping(1) -- Затухание при движении по инерции
	ship.body:setAngularDamping(2) -- Затухание при повороте

	-- Турель и стрельба
	ship.turelAngle = 0
	ship.shootTimer = 0
	ship.shootDelay = 0.2

	-- Прочность
	ship.health = 100
	ship.energy = 100
	ship.isDead = false

	-- Тип корабля
	ship.type = 'none'

	-- Валидация угла (в радианах)
	function ship:validateAngle(angle)
		while angle < 0 do angle = angle + math.pi * 2 end
		while angle >= math.pi * 2 do angle = angle - math.pi * 2 end
		return angle
	end

	-- Передвижение
	function ship:move(dx)
		self.body:applyForce(dx * self.movementSpeed * math.cos(self.body:getAngle()), dx * self.movementSpeed * math.sin(self.body:getAngle()))
	end

	-- Стрельба
	function ship:shoot(bullet)
		if self.shootTimer > self.shootDelay then
			table.insert(objects, bullet)
			self.shootTimer = 0
		end
	end

	function ship:destroy()
		for i, object in ipairs(objects) do
			if object == self then
				objects[i] = nil
				table.remove(objects, i)
			end
		end
	end

	function ship:kill()
		self:destroy()
	end

	-- Функция урона корабля
	function ship:hit(angle, value)

		local ik = value * 1.8
		local ix, iy = math.cos(angle) * ik, math.sin(angle) * ik
		self.body:applyLinearImpulse(ix, iy)

		-- Звук урона
		if sounds.ship_damage:isPlaying() then
			love.audio.play(sounds.ship_damage1)
		else
			love.audio.play(sounds.ship_damage)
		end


		if not self.isDead then -- если еще не мертв
			-- Толкаем корабль
			self.health = self.health - value -- отнимаем от прочности value
			if (self.health <= 0) then -- если прочность <= 0
				self.health = 0
				self.isDead = true
				self:kill()
			end
		end
	end

	return ship
end
